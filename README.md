**# Lambda-Layer-creation-Terraform**

This will create the lambda function and lambda layer 

_**Pre-Requisite**_

1: Terraform >= v0.15.4 version needs to be installed 
2: IAM Role for dev team which should have full programetic access for lambda
3: Aws CLI >= 2 version for AWS cofigure 
    eg : aws configure 
        AWS Access Key ID [ your access key]: 
        AWS Secret Access Key [ your secret key]: 
        Default region name [ Region in which lambda function needs to create ]: 
        Default output format [txt or json ]:


**Steps:**__

1: Clone this project to your local machine 

run terraform init to initialize.

run terraform apply to see it work.

run terraform destroy to clean up.
